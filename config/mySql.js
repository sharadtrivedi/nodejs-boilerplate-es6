import mysql from 'mysql';
import winston from 'winston';


const pool = mysql.createPool({
    connectionLimit: 10,
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
});

pool.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            winston.error('MySql connection was closed.')
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            winston.error('MySql has too many connections.')
        }
        if (err.code === 'ECONNREFUSED') {
            winston.error('MySql connection was refused.')
        }
    }
    if (connection)  connection.release();
    winston.info('MySql connected successfully');
    return
});

module.exports = pool ;