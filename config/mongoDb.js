import mongoose from 'mongoose';
import winston from 'winston';

let connection = mongoose.connection;

connection.on('error', function(error){
	winston.error(error);
});

connection.once('open',function(){
	winston.info('Connected to MongoDb.');
});

connection.on('disconnected', function(){
	//Reconnect
	mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true ,
		socketTimeoutMS: 90000,
		keepAlive: true,
		autoReconnect: true,
		reconnectInterval: 500, // in 500ms 
		reconnectTries: 30, // 30 times  
		poolSize: 5, //
		bufferMaxEntries: 0
	})
}, function (err) {
	if(err)
		winston.error(err);
	else
		winston.info('MongoDb reconnected after disconnection.');

	connection = mongoose.connection;
});

var gracefulExit = function () {
	mongoose.connection.close(function () {
		winston.info('MongoDb is disconnected through app termination')
		process.exit(0);
	});
}

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', gracefulExit);
process.on('SIGTERM', gracefulExit);

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true ,
	socketTimeoutMS: 90000,
		keepAlive: true,
		autoReconnect: true,
		reconnectInterval: 500, // in 500ms 
		reconnectTries: 30, // 30 times  
		poolSize: 5, //
		bufferMaxEntries: 0
}, function (err) {
	if(err)
		winston.info('MongoDb first time connection Error:' + err);
	else
		winston.info('MongoDb first time connected');
});