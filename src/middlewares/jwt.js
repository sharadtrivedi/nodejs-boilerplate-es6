import jwt from 'jsonwebtoken';
import * as errors from '../utils/errors';
import crypt from '../utils/enDec';

let jwtKey ;

class JwtToken{
  constructor() { }

  async generateToken(tokenData, exp, routeUrl){
    // Get JWT Keys.
    jwtKey = (new JwtToken()).jwtKeys(routeUrl);
    return new Promise((resolve,reject)=>{
      jwt.sign(tokenData, jwtKey, { expiresIn: exp }, (err, token) => {
        if(err) {
          return reject(new errors.OperationalError('Could not generate Token'));
        }
        token =  crypt.encryptData(token);
        return resolve(token) ;
      });
    })
  }

  async verifyToken(req, res, next){
    
    let token = req.headers.authorization;
    
    if(!token) return next(new errors.AuthorizationError('Did not receive token'));
    token = crypt.decryptData(token);
    
    // Get JWT Keys.
    jwtKey = (new JwtToken()).jwtKeys(req.originalUrl);

    jwt.verify(token, jwtKey, (err, decoded) => {
      if(err) {
        next(new errors.AuthorizationError('Token Invalid or Expired. Forbidden!'));
      }
      req._decoded = decoded ;
      next();
    });
  }

  jwtKeys(routeUrl){
    // add different secret keys for different routes.
    if(routeUrl === '/v1/sample/verifyToken') return  process.env.SAMPLE_JWTKEY ;
    if(routeUrl === '/v1/sample/sendToken') return  process.env.SAMPLE_JWTKEY ;
    if(routeUrl === '/v1/sample/demo') return  process.env.SAMPLE_JWTKEY ;
  }
}

module.exports = new JwtToken();