import crypto from 'crypto' ;
const KEY = process.env.CRYPTKEY; 

class Crypto{
    constructor(){ }

    encryptData(data){
        let encrypt = crypto.createCipheriv('des-ede3', KEY, "");
        let theCipher = encrypt.update(data, 'utf8', 'base64');
        theCipher += encrypt.final('base64');
        return theCipher ;
    }

    decryptData(encryptedData){
        let decrypt = crypto.createDecipheriv('des-ede3', KEY, "");
        let str = decrypt.update(encryptedData, 'base64', 'utf8');
        return (str + decrypt.final('utf8'));
    }
}
module.exports = new Crypto();